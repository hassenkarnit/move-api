## **API Endpoints**

**_User_**

    interface User {
    	email : string,
    	password : string,
    	firstName : string,
    	lastName : string,
    	country:  string
    	phoneNumber ?: string,
        avatar?: string
    }

- (GET) /users : () => User[] (get all users)
- (GET) /users/{id}: () => User (get user by id)
- (POST) /users : (user: User) => User (created user)
- (POST) /users/forgot-password : ({email : string}) => void
- (POST) /users/reset-password: ({passwordResetToken : string, newPassword: string}) => void
- (POST) /users/sign-in : ({email: string, password : string}) => User
- (PUT) /users/{id} : (User) => User (update user by id)
- (DELETE) /users/{id} => () => void (delete user by id)
- (GET) /users/refresh-token => () => accessToken : string (refresh access token by passing refreshtoken in authorization header)
- (GET) /users/verify-token => () => {token: {isValid : boolean}} // token is to be passed in the cookies
