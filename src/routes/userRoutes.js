import * as AuthMiddleware from 'Middlewares/auth';
import * as UploadMiddleware from 'Middlewares/upload';
import * as userController from 'Controllers/userController';
import * as userMiddleware from 'Middlewares/user';
import * as userValidator from 'Validators/user';
import { Router } from 'express';
import upload from 'Helpers/defaultMulterConfig';

const router = Router();

const API_ROUTE_HEADER = '/users';

router.post(
  API_ROUTE_HEADER,
  upload.single('avatar'),
  userValidator.createUser,
  userMiddleware.checkIfEmailExists,
  userController.signUp,
  UploadMiddleware.moveFileToUserFolder,
);
router.get(`${API_ROUTE_HEADER}/activate-email`, userMiddleware.checkEmailActivationToken, userController.activateAccount);
router.put(
  `${API_ROUTE_HEADER}/update-password`,
  userValidator.updatePassword,
  AuthMiddleware.currentUserWithPassword,
  userMiddleware.signIn,
  userController.updatePassword,
);
router.put(
  `${API_ROUTE_HEADER}/update-avatar`,
  upload.single('avatar'),
  AuthMiddleware.currentUser,
  userMiddleware.updateAvatar,
  UploadMiddleware.createUserAvatar,
  userController.updateUser,
);
router.post(`${API_ROUTE_HEADER}/forgot-password`, userValidator.checkEmail, userMiddleware.findByEmail, userController.forgotPassword);
router.post(`${API_ROUTE_HEADER}/reset-password`, userValidator.resetPassword, userMiddleware.checkPasswordResetToken, userController.resetPassword);
router.post(`${API_ROUTE_HEADER}/sign-in`, userValidator.signIn, userMiddleware.findByEmail, userMiddleware.signIn, userController.signIn);
router.get(`${API_ROUTE_HEADER}/verify-token`, userController.verifyUserToken);
router.post(`${API_ROUTE_HEADER}/sign-out`, AuthMiddleware.currentUser, userController.signOut);
router.get(`${API_ROUTE_HEADER}/refresh-token`, AuthMiddleware.findByRefreshToken, userController.refreshToken);
router.delete(`${API_ROUTE_HEADER}/:id`, userController.deleteUserById);
router.get(`${API_ROUTE_HEADER}/by-token`, AuthMiddleware.currentUser, userController.fetchByToken);
router.put(`${API_ROUTE_HEADER}/:id`, userController.updateUserById);
router.get(`${API_ROUTE_HEADER}/:id`, userController.getUserById);
router.put(API_ROUTE_HEADER, userValidator.updateUser, AuthMiddleware.currentUser, userController.updateUser);
router.get(API_ROUTE_HEADER, userController.getAllUsers);

export default router;
