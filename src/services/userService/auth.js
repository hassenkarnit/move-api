import * as TokenService from 'Services/tokenService';
import User from 'Models/User';
import { hashSync } from 'bcrypt';
import mailer from 'Helpers/mailer';

export const hashPassword = password => hashSync(password, 12);

// const signupMailConstructor = emailActivationToken => {
//   const emailActivationRoute = `${process.env.API_HOST}${process.env.BASE_API_URL}/users/activate-email?token=${emailActivationToken}`;
//   const emailSubject = 'Activation de votre compte El Business';
//   const emailHtmlBody = `<div><h1>El Business</h1><p>Veuillez cliquer <a href=${emailActivationRoute}>ici</a> afin
//     d'activer votre compte</p></div>`;
//   return { emailSubject, emailHtmlBody };
// };

// TODO: ENABLE MAILER

export const signUp = async (userData, res, next) => {
  const { password } = userData;
  const hashedPassword = hashPassword(password);
  userData.password = hashedPassword;
  try {
    const user = await new User(userData).save();
    const emailActivationToken = TokenService.createToken(user.id, process.env.EMAIL_ACTIVATION_SECRET_KEY);
    user.emailActivationToken = emailActivationToken;
    await user.save();
    const updatedUser = await createOrRefreshUserToken(user);
    // const { emailSubject, emailHtmlBody } = signupMailConstructor(emailActivationToken);
    // await mailer(email, emailSubject, emailHtmlBody);
    res.locals.currentUser = updatedUser;
    next();
  } catch (error) {
    res.status(404).send(error);
  }
};

export const activateAccount = async res => {
  try {
    const { user } = res.locals;
    user.activated = true;
    user.emailActivationToken = null;
    await user.save();
    res.send('Account activated, you can now log in to Oryx Guide');
  } catch (error) {
    res.status(400).send(error);
  }
};

export const forgotPassword = async res => {
  const { user } = res.locals;
  const resetPasswordToken = TokenService.createToken(user.id, process.env.PASSWORD_RESET_SECRET_KEY);
  user.resetPasswordToken = resetPasswordToken;
  await user.save();
  const resetPasswordRoute = `${process.env.FRONT_END_URI}/resetpassword?token=${resetPasswordToken}`;
  const emailSubject = 'Réinitialisation de votre mot de passe Oryx Guide';
  const emailHtmlBody = `<div><h1>El Business</h1><p>Veuillez cliquer <a href=${resetPasswordRoute}>ici</a> afin 
  de réinitialiser votre mot de passe</p></div>`;
  await mailer(user.email, emailSubject, emailHtmlBody);
  res.send('Password reset request success');
};

export const resetPassword = async (newPassword, res) => {
  try {
    const { user } = res.locals;
    const hashedPassword = hashPassword(newPassword);
    user.password = hashedPassword;
    user.resetPasswordToken = null;
    await user.save();
    res.send('Password reset successfully');
  } catch (error) {
    res.status(400).send(error);
  }
};

export const signIn = async res => {
  const { user } = res.locals;
  const updatedUser = await createOrRefreshUserToken(user);

  res.send(updatedUser);
};

export const signOut = async res => {
  const { currentUser } = res.locals;
  currentUser.token = null;
  try {
    await currentUser.save();
    res.send('User logged out');
  } catch (error) {
    res.status(400).send(error);
  }
};

const createOrRefreshUserToken = async user => {
  const { token } = user;
  if (!token || !token.refreshToken) {
    user.token.refreshToken = TokenService.createToken(user.id, process.env.REFRESH_TOKEN_SECRET_KEY, false);
  }
  if (!token || !token.accessToken || !TokenService.isAccessTokenStillValid(token.accessToken)) {
    user.token.accessToken = TokenService.createToken(user.id, process.env.ACCESS_TOKEN_SECRET_KEY);
  }
  const updatedUser = await user.save();
  return updatedUser;
};

export const refreshToken = async res => {
  const { currentUser } = res.locals;
  const accessToken = TokenService.createToken(currentUser.id, process.env.ACCESS_TOKEN_SECRET_KEY);
  try {
    currentUser.token.accessToken = accessToken;
    await currentUser.save();
    res.send(accessToken);
  } catch (error) {
    res.status(400).send({ message: 'Something went wrong', error });
  }
};
