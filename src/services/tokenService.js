import { sign, verify } from 'jsonwebtoken';


export const isAccessTokenStillValid = token => verify(token, process.env.ACCESS_TOKEN_SECRET_KEY, error => {
  if (error && error.name === 'TokenExpiredError') return false;
  return true;
});


export const createToken = (id, secret, isExpireble = true) => sign(
  { id },
  secret,
  isExpireble && {
    expiresIn: process.env.TOKEN_EXPIRES_IN,
  },
);
