import { verify } from 'jsonwebtoken';


const verifyToken = (token, secret) => verify(token, secret);


export default verifyToken;
