/* eslint-disable no-console */
import { createTransport } from 'nodemailer';
import sendgridTransport from 'nodemailer-sendgrid-transport';

require('dotenv').config();

const transporter = createTransport(
  sendgridTransport({
    auth: {
      api_key: process.env.EMAIL_API,
    },
  }),
);

const mailer = async (receiverEmail, subject, htmlBody) => {
  await transporter
    .sendMail({
      from: '"Oryx Guide" <no-reply@oryxguide.com>',
      to: receiverEmail,
      subject,
      html: htmlBody,
    })
    .catch(error => console.log(error));
};

export default mailer;
