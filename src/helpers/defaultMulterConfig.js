import multer from 'multer';

require('dotenv').config();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, `${process.env.IMAGE_STORAGE_LOCATION}/tmp`);
  },
  filename: (_req, file, cb) => cb(null, `${Date.now()}`),
});

const fileFilter = (_req, file, cb) => {
  if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg') {
    cb(null, true);
  } else {
    cb(new Error(`${file.mimetype} type is not accepted.`), false);
  }
};

export default multer({ storage, fileFilter });
