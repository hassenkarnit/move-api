import * as TokenService from 'Services/tokenService';
import * as UserService from 'Services/userService';

export const signUp = (req, res, next) => {
  UserService.signUp(req.body, res, next);
};
export const signIn = (req, res) => {
  UserService.signIn(res);
};

export const activateAccount = (req, res) => {
  UserService.activateAccount(res);
};

export const forgotPassword = (req, res) => {
  UserService.forgotPassword(res);
};

export const resetPassword = (req, res) => {
  UserService.resetPassword(req.body.password, res);
};

export const getAllUsers = (req, res) => {
  UserService.getAllUsers(res);
};

export const getUserById = (req, res) => {
  UserService.getUserById(req.params.id, res);
};

export const deleteUserById = (req, res) => {
  UserService.deleteUserById(req.params.id, res);
};

export const updateUserById = (req, res) => {
  UserService.updateUserById(req.params.id, req.body, res);
};

export const signOut = (req, res) => {
  UserService.signOut(res);
};

export const verifyUserToken = (req, res) => {
  const token = req.headers.authorization;
  const isValid = token && TokenService.isAccessTokenStillValid(token.accessToken);
  res.send({ token: { isValid } });
};

export const refreshToken = (req, res) => {
  UserService.refreshToken(res);
};

export const fetchByToken = (req, res) => {
  res.send(res.locals.currentUser);
};

export const updateUser = (req, res) => {
  UserService.updateUser(res);
};

export const updatePassword = (req, res) => {
  UserService.updatePassword(req.body, res);
};
