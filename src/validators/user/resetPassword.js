const resetPassword = (req, res, next) => {
  const { password, resetPasswordToken } = req.body;
  if (!password || !resetPasswordToken) {
    res.status(422).send('Missing required information');
    return;
  }
  next();
};

export default resetPassword;
