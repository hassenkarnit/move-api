import isEmail from 'Helpers/isEmail';

const signIn = (req, res, next) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(422).send('Missing required information');
    return;
  }
  if (!isEmail(email)) {
    res.status(422).send('Wrong email format');
    return;
  }
  next();
};

export default signIn;
