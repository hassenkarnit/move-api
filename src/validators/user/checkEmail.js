import isEmail from 'Helpers/isEmail';

const checkIfEmailPresent = (req, res, next) => {
  const { email } = req.body;
  if (!email) {
    res.status(422).send('Email is required');
    return;
  }
  if (!isEmail(email)) {
    res.status(422).send('Wrong email format');
    return;
  }
  next();
};

export default checkIfEmailPresent;
