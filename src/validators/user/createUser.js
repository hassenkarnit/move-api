import isEmail from 'Helpers/isEmail';

const createUser = (req, res, next) => {
  const {
    email, password, firstName, lastName, country, type,
  } = req.body;
  if (!email || !password || !firstName || !lastName || !country || !type) {
    res.status(422).send('Missing required information');
    return;
  }
  if (!isEmail(email)) {
    res.status(422).send('Wrong email format');
    return;
  }
  next();
};

export default createUser;
