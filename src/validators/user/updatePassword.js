const updatePassword = (req, res, next) => {
  const { password, newPassword } = req.body;
  if (typeof password !== 'string' || typeof newPassword !== 'string') {
    res.status(422).send('Password required');
    return;
  }
  next();
};
export default updatePassword;
