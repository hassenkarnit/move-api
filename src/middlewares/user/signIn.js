import { compareSync } from 'bcrypt';

const signIn = async (req, res, next) => {
  const { password } = req.body;
  const {
    approved, activated, password: userPassword, banned,
  } = res.locals.user || res.locals.currentUser;

  try {
    checkVerifications(approved, activated, banned);
    if (!compareSync(password, userPassword)) {
      throw Error('Password incorrect');
    }
    next();
  } catch (error) {
    res.status(401).send(error);
  }
};

const checkVerifications = (approved, activated, banned) => {
  if (banned) throw Error('User banned');
  if (!activated) throw Error('Account not activated, please check your email inbox.');
  if (!approved) throw Error('Please wait for an admin to approve your account');
};

export default signIn;
