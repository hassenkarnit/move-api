import User from 'Models/User';
import verifyToken from 'Helpers/verifyToken';

const checkPasswordResetToken = async (req, res, next) => {
  const { token } = req.body;
  try {
    const decodedToken = verifyToken(token, process.env.PASSWORD_RESET_SECRET_KEY);
    const user = await User.findById(decodedToken.id);
    if (!user) {
      res.status(404).send('Problem while finding user');
      return;
    }
    res.locals.user = user;
    next();
  } catch (error) {
    if (error.name === 'TokenExpiredError') {
      res.status(403).send('Token expired');
      return;
    }
    res.status(500).send('Something went wrong');
  }
};

export default checkPasswordResetToken;
