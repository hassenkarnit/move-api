import User from 'Models/User';

const findByEmail = async (req, res, next) => {
  const { email } = req.body;
  const emailRegex = new RegExp(`^${email}$`, 'i');

  const user = await User.findOne({ email: { $regex: emailRegex } });
  if (!user) {
    res.status(404).send('Problem while finding user');
    return;
  }
  res.locals.user = user;
  next();
};

export default findByEmail;
