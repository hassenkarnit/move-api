import checkEmailActivationToken from 'Middlewares/user/checkEmailActivationToken';
import checkIfEmailExists from 'Middlewares/user/checkIfEmailExists';
import checkPasswordResetToken from 'Middlewares/user/checkPasswordResetToken';
import findByEmail from 'Middlewares/user/findByEmail';
import signIn from 'Middlewares/user/signIn';
import updateAvatar from 'Middlewares/user/updateAvatar';

export {
  checkEmailActivationToken, checkIfEmailExists, checkPasswordResetToken, signIn, findByEmail, updateAvatar,
};
